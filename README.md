# runsv

Applications rely on different services like databases or message queues. Before your application can do any work you need to connect to those services. Runsv will help you with that💪.

![npm](https://img.shields.io/npm/v/runsv?style=for-the-badge)
[![pipeline status](https://gitlab.com/runsvjs/runsv/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/runsv/commits/master)
[![coverage report](https://gitlab.com/runsvjs/runsv/badges/master/coverage.svg)](https://gitlab.com/runsvjs/runsv/commits/master)

## Features

* Callback, Promises and Async interfaces
* Detect when services got stuck on start/stop 
* Monitor start/stop time for each service
* Mocha/Jest friendly [see example](examples/mocha.js)
* Detect service circular dependency


```javascript
const runsv = require('runsv').create();

// Require your defined services
const postgresql = require('runsv-pg')();
const redis = require('runsv-redis')();
const app = require('./my-web-app-service');

// your web app requires pg and redis
runsv.addService(app, pg, redis); 

// start order: redis → pg → app
runsv.start(function (err, clients) {
    // pg, redis and app are ready and running
    const {pg, redis} = clients;
    // do your magic here...
});
process.once('SIGTERM', function(){
    // stop order: app → pg → redis
    runsv.stop();
});
```

`runsv.addService(app, pg, redis)` adds services `app`, `pg` and `redis`. It also defines that `app` requires `pg` and `redis`.  
`runsv` will create a dependency graph and start/stop them in the correct order.  

```javascript
// complex service scenario
runsv.addService(a, b, c); // a needs b and c
runsv.addService(z, a); // z needs a
runsv.addService(x, c); // x needs c
// start order: b → x → c → a → z
// stop order: z → a → c → x → b

```
Services that rely on another services will have access to their clients at `start` time. See services definition below.

## Services

A service is just an object with the following interface:

* `name` Service name. A string
* `start (dependencies, callback)` A function that starts the service
    * `callback(err)` when service is ready. Pass an error if something went wrong
* `stop (callback)` A function that stops the service
    * `callback(err)` when service has stopped. Pass an error if something went wrong
* [OPTIONAL] `getClient()` A function that returns the client. I.e a database client

### Example: A redis service wrapper

```javascript
const redis = require('redis');

function createRedisService(redisConf) {
    let client;

    // #name will be used by other services/code to access this service client
    const name = 'redis';

    function start(callback) {
        client = redis.createClient(redisConf);
        // callback once the client is ready
        client.once('ready', function(err){
            return callback(err); 
        });
    }

    function stop(callback) {
        if(!client){
            // nothing to do here
            return callback();
        }
        client.quit(function (err) {
            client = null;
            return callback(err);
        });
    }

    function getClient() {
        return client;
    }

    return {
        name,
        start,
        stop,
        getClient
    };
}
```

### Example: A service with dependencies

```javascript

// a service that relies on the redis service
function createComplexService() {
    let client;
    const name = 'complex';

    function start(dependencies, callback) {
        const redisClient = dependencies.redis;
        client = createClient(redisClient);
    }

    function stop(callback) {
        client = null;
        return callback();
    }

    function getClient() {
        return client;
    }

    return {
        name,
        start,
        stop,
        getClient
    };
}
//...
let redis = createRedisService();
let complex = createComplexService();
sv.addService(complex, redis); // complex relies on redis
sv.start(/*...*/);

```

## Callback, Promises or Async interfaces

Services can be defined with any API style: callback, promises or async/await.  
RunSV can be consumed with callbacks, promises or async/await as well.  

### Async/promises interface

To create this interface just call `async()` on the runsv object.

```javascript
const runsv = require('runsv').create().async();
// ...
await runsv.start();
// or, with promises
runsv.start()
    .then(clients => /*...*/)
    .catch(error => /*...*/);
```

### Define async services

You can define services with callback, promises or async/await interface styles.  

```javascript
// async example
const myService = {
	name: 'myService',
	async start(deps){ /**/}
	async stop(){ /**/}
}

```
ℹ️  You can mix services with different interfaces

## API

* `getService(name)` Get a service by its name
* `addService(service, [...dependencies])` Adds a service with optional dependencies
* `listServices()` Gets a list of services i.e `['pg', 'redis']`
* `getClients(...only)` Get a bunch of clients, If no client is specified it returns all clients
* `start(callback)` Start all services
* `stop(callback)` Stop all services
* `async()` returns an async/await interface

### Events
* start(name) Service *name* has started
* stop(name) Service *name* has stopped
* waiting(name, event, time) Waiting for *name* to *event* for *time* milliseconds

```javascript
// Events example
runsv.addService(app, pg, redis); // app requires pg and redis

// Log service start. I.e print "pg ready. Took 10ms to become ready"
runsv.on('start', (service, res) => console.log(service, 'ready.', 'Took', res.took+'ms to become ready'));

// Log services that are not starting/stopping. I. waiting for pg to start (200ms)
runsv.on('waiting', (service, event, ms) => console.log(`waiting for ${service} to ${event} (${ms}ms)`));

// Log service stop
runsv.on('stop', (service, res) => console.log(service, 'stopped', 'took', res.took+'ms to stop'));
runsv.start(/*...*/);
```

### Hooks: setup/teardown

* *setup hook* runs as soon as the service is ready
* *teardown hook* runs just before the service is stopped

A shared context is passed to every hook.  
You can augment that context to share information between hooks. 
**Shared context is not shared with services.**  

Hooks can use callback, promise or async/await API.

> Do not pass [promisified](https://nodejs.org/api/util.html#utilpromisifyoriginal) functions as runsv might not 
> be able to properly handle them.

`addService(service, [,deps])` returns an array with two hooks for the added service.

```javascript
const [setupPG, teardownPG] = runsv.addService(pg);
runsv.addService(app, pg, redis); // app requires pg and redis

setupPG(function(ctxt, deps, callback){ // could also be setupPG(async function(ctxt, deps))
    // ctxt is a shared context between all hooks
    ctxt.database = 'my-db-' + Date.now(); // create a random db
    deps.pg.query(`create database ${ctxt.database};`, callback);
});

teardownPG(function(ctxt, deps, callback){
    deps.pg.query(`drop database ${ctxt.database};`, callback);
});

```

## Existing services

Some wrappers for popular node modules:

### Databases
* [CouchDB](https://gitlab.com/runsvjs/couchdb) A service wrapper around `nano`
* [PostgreSQL](https://gitlab.com/runsvjs/pg) A `pg` service wrapper
* [Redis](https://gitlab.com/runsvjs/redis) A `redis` service wrapper

### Other

* [http](https://gitlab.com/runsvjs/http) [nodejs http server](https://nodejs.org/api/http.html#http_class_http_server)

[See more usage examples](./examples/)
