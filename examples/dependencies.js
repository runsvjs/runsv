'use strict';
/* 
 * How to run this example:
 * From the root of the project run 
 * $ node examples/dependencies.js
 *
 * Expected output:
 * myComplex say:  hello world
 */
const {
	create
} = require('..');

const simpleBlaBla = {
	name: 'simple',
	getClient (){
		return {
			say (){
				return 'hello';
			}
		};
	},
	start (_, callback){
		return callback();
	},
	stop (callback){
		return callback();
	}
};

const complex = function (){
	const ret =  {
		name: 'complex',
		getClient (){
			return {
				say (){
					return ret.simpleClient.say() + ' world';
				}
			};
		},
		start ({ simple }, callback){
			// the simpleBlaBla object has been named "simple" 
			// we always use the #name property of a service to access it
			ret.simpleClient = simple;
			return callback();
		},
		stop (callback){
			return callback();
		}
	};
	return ret;
}();

const runsv = create();
// complex relais on simpleBlaBla
runsv.addService(complex, simpleBlaBla);

runsv.start(function (){
	// fetch the complex client as myComplex
	const clients = runsv.getClients();
	console.log('myComplex say: ', clients.complex.say());
});
