'use strict';
/* 
 * How to run this example:
 * From the root of the project run 
 * $ mocha examples/mocha.js
 *
 * Expected output:
 * A mocha test passing
 */

const assert = require('assert');
const {
	create
} = require('..');

// mock service
function createService (name) {

	const client = {
		name,
		query (cmd, callback){
			client.logs.push(cmd);
			return callback();
		},
		logs: []
	};
	const ret = {
		name,
		start (_, callback) {
			return callback();
		},
		getClient () {
			return client;
		},
		stop (callback) {
			return callback();
		}
	};
	return Object.freeze(ret);
}

function createSandbox (...services){
	before(function withServicesBeforeHook (done) {
		const self = this;
		self.services = create();
		self.services.addService(...services);
		self.services.start(function (err){
			if(err){
				self.services.stop(function (){
					return done(err);
				});
			}
			return done();
		});
	});
}

describe('Some test',function (){
	// [Setup] Create your sandbox
	createSandbox(createService('mysql'), createService('redis'));
	// [Setup] Do some setup
	before(function someSetup (done){
		// Create some test database
		this.randomDBname = 'test' + Date.now();
		// get previously created mysql client
		const { mysql } = this.services.getClients();
		mysql.query(`CREATE DATABASE ${this.randomDBname};`, done);

	});
	// [Teardown] Clean up after your test
	after(function rmTempDatabase (done){
		const { mysql } = this.services.getClients();
		const { randomDBname } = this;
		mysql.query(`DROP DATABASE ${randomDBname};`, done);

	});
	// [Teardown] Stop services
	after(function (done){
		this.services.stop(done);
	});
	it('should work', function (){
		const { mysql } = this.services.getClients('mysql');
		// Some test code
		assert(mysql.name === 'mysql');
	});
});
