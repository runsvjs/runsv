'use strict';

const express = require('express');
const session = require('express-session');

function makeApp ({ redis }){
	const app = express();

	const RedisStore = require('connect-redis')(session);
	app.use(session({
		store: new RedisStore({ client: redis }),
		secret: 'keyboard cat',
		resave: false,
	}));

	function hello (req, res){
		req.db.hello(function (err, result){
			if(err){
				res.status(500);
				res.end(err.message);
				return;
			}
			console.log('rows', result);

			return res.json(result);
		});
	}
	app.get('/', hello);
	return app;
}

exports = module.exports = makeApp;
