'use strict';

function select1 (callback){
	return function select1impl (err, res){
		if(err){
			return callback(err);
		}
		return callback(null, res.rows[0]);
	};
}

function curry (fn, ...a){
	return function (...b){
		return fn(...a, ...b);
	};
}

function hello (pg, callback){
	const query = 'SELECT $1::text as message';
	const params = ['Hello world'];
	pg.query(query, params, select1(callback));
}

function create (pg){
	return Object.freeze({
		hello: curry(hello, pg)
	});
}
exports = module.exports = create;
