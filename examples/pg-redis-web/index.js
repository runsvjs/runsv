'use strict';
require('dotenv').config();
const { createHttpTerminator } = require('http-terminator');
const runsv = require('runsv').create();
const pg = require('runsv-pg')();
const redis = require('runsv-redis')(process.env.REDIS);
const makeApp = require('./app');
const db = require('./db');
const { callbackify } = require('util');

const http = require('http');

const server = http.createServer();
const httpTerminator = createHttpTerminator({ server });

const terminate = callbackify(httpTerminator.terminate).bind(httpTerminator);
runsv.addService(pg, redis);

process.on('SIGINT', function (){
	console.log('SIGINT received');
	terminate(function (err){
		if(err){
			console.error(err);
		}
		runsv.stop(function (err){
			if(err){
				console.error(err);
			}
			process.exit(0);
		});
	});
});

runsv.start(function (err, clients){
	if(err){
		console.error('could not start:' + err.message);
		console.trace(err.stack);
		process.exit(1);
	}
	const  { pg, redis }  = clients;
	let req = http.IncomingMessage.prototype;
	// augment the IncomingMessage so middleware can access our db
	req.db = db(pg);
	const app = makeApp({ redis });

	server.on('request', app);
	server.listen(3000, function (){
	// Tell PM2 that we are ready
		if(process.send){
			process.send('ready');
		}
		console.log('ready');
	});
});
runsv.on('start', (name, took) => console.log('started', name, 'took', took, 'ms'));
runsv.on('stop', (name, took) => console.log('stopped', name, 'took', took, 'ms'));
