'use strict';
const {
	DepGraph
} = require('dependency-graph');
const EventEmitter = require('events');
const util = require('util');
const assert = require('assert');
const { isAsync } = require('./lib/util');
const { callbackify } = require('util');

const WAIT_FOR_DATA = Symbol('waitingFor');
const WaitingFor = require('./lib/waiting-for');

function isValidService (input) {
	assert(input.name, 'service must have a #name');
	const name = input.name;
	assert(input.start, `service ${name} must have a #start(dependencies, callback) function`);
	assert(input.stop, `service ${name} must have a #stop function`);
	assert.deepStrictEqual(typeof input.start, 'function', `${name} #start must be a function`);
	assert.deepStrictEqual(typeof input.stop, 'function', `${name} #stop must be a function`);
}

function RunSV (options) {
	let myOptions = {
		emitWaitingEvery: 1000,
		...options || {},
	};
	const self = this;
	self.setupHooks = new Map();
	self.teardownHooks = new Map();
	self.context = null;
	self.services = new Map();
	self.dependencies = new DepGraph();
	self[WAIT_FOR_DATA] = new WaitingFor(function (name, event, time){
		self.emit('waiting', name, event, time);
	}, myOptions.emitWaitingEvery);
	EventEmitter.call(self);
}
util.inherits(RunSV, EventEmitter);
RunSV.prototype.addService = function addServiceImpl (service, ...dependencies) {
	var self = this;
	for (const s of [service, ...dependencies]) {
		isValidService(s);
		if (self.services.has(s.name)) {
			continue;
		}
		self.services.set(s.name, s);
		self.dependencies.addNode(s.name);
	}
	for (const dep of dependencies) {
		self.dependencies.addDependency(service.name, dep.name);
	}
	function hookSetup (fn){
		if(self.setupHooks.has(service)){
			throw new Error('A setup hook has been already registered for service: ' + service.name);
		}
		self.setupHooks.set(service, fn);
	}
	function hookTeardown (fn){
		if(self.teardownHooks.has(service)){
			throw new Error('A teardown hook has been already registered for service: ' + service.name);
		}
		self.teardownHooks.set(service, fn);
	}
	return [hookSetup, hookTeardown];
};
RunSV.prototype.getService = function getService (name) {
	assert(!!name, 'service name is required');
	return this.services.get(name);
};
RunSV.prototype.listServices = function listServices () {
	return Array.from(this.services.keys());
};
RunSV.prototype.getClients = function getClients (...only) {
	const ret = {};
	const names = (only.length && only) || Array.from(this.services.keys());
	// populate the result with existing clients
	for (const n of names) {
		const service = this.services.get(n);
		if (service.getClient) {
			ret[n] = service.getClient();
		}
	}
	return ret;
};
RunSV.prototype.startPlan = function startPlan () {
	return this.dependencies.overallOrder();
};
RunSV.prototype.init = function init (callback) {
	return this.start(callback);
};

RunSV.prototype.start = function start (callback) {
	let i = 0;
	const self = this;
	self.context = {};
	assert(callback, 'callback is required');
	let plan;
	try {
		plan = self.startPlan();
	} catch (e) {
		// typically a circular dependency
		return callback(e);
	}

	let now = Date.now();

	function next (err, result) {
		const previous = plan[i - 1];

		function bail (err){
			self[WAIT_FOR_DATA].stop();
			err.message = `Could not start ${previous}. Reason: ${err.message}`;
			return callback(err);
		}

		if (err) {
			return bail(err);
		}

		function runService (){

			const name = plan[i++];

			if (!name) {
				return callback(null, self.getClients());
			}
			const service = self.services.get(name);
			const listOfdependencies = self.dependencies.dependenciesOf(name);
			const dependencies = listOfdependencies.length ? self.getClients(...listOfdependencies) : null;
		
			// Start the "waiting for" check interval
			self[WAIT_FOR_DATA].start(name, 'start');

			if(isAsync(service.start)){
				callbackify(service.start).bind(service)(dependencies, next);
			}else {
				service.start(dependencies, next);
			}
		}

		function runPreviousHook (previous, callback){
			const service = self.services.get(previous);
			let hook = self.setupHooks.get(service);
			if(!hook){
				setImmediate(callback);
			}
			// Start the "waiting for" check interval
			self[WAIT_FOR_DATA].start(service.name + ':hook:setup', 'start');
			const dependencies = self.dependencies.dependenciesOf(previous);
			const clients = self.getClients(previous,...dependencies);
			if(isAsync(hook)){
				callbackify(hook).bind(hook)(self.context, clients, callback);
			}else{
				hook(self.context, clients, callback);
			}
		}
		if (previous) {
			self[WAIT_FOR_DATA].stop();
			result = result || {};
			result.took =Date.now() - now;
			self.emit('start', previous, result);
			now = Date.now();
			let hook = self.setupHooks.get(self.services.get(previous));
			if(!hook){
				return runService();
			}
			runPreviousHook(previous, function (err){
				self[WAIT_FOR_DATA].stop();
				if(err){
					return bail(err);
				}
				runService();
			});
		}else{
			runService();
		}
	}
	next();
};
RunSV.prototype.stopPlan = function stopPlan () {
	return this.dependencies.overallOrder().reverse(); // stop the services in reverse order
};
RunSV.prototype.stop = function stop (callback) {
	var i = 0;
	const self = this;
	assert(callback, 'callback is required');
	const plan = self.stopPlan();

	let now = Date.now();

	function next (err, result) {
		self[WAIT_FOR_DATA].stop();
		const previous = plan[i - 1];

		if (previous) {
			result = result || {};
			result.took = Date.now() - now;
			self.emit('stop', previous, result);
			now = Date.now();
		}

		function bail (err){
			self[WAIT_FOR_DATA].stop();
			err.message = `Could not stop ${previous}. Reason: ${err.message}`;
			return callback(err);
		}

		if (err) {
			return bail(err);
		}

		const name = plan[i++];

		if (!name) {
			return callback();
		}
		const service = self.services.get(name);

		function runTeardownHook (service, callback){
			if(!service){
				setImmediate(callback);
			}
			let hook = self.teardownHooks.get(service);
			if(!hook){
				setImmediate(callback);
				return;
			}
			// Start the "waiting for" check interval
			self[WAIT_FOR_DATA].start(service.name + ':hook:teardown', 'start');
			const deps = self.dependencies.dependenciesOf(name);
			const clients = self.getClients(service.name, ...deps);
			
			if(isAsync(hook)){
				callbackify(hook).bind(hook)(self.context, clients, callback);
			}else{
				hook(self.context, clients, callback);
			}
		}

		function stopService (){
			self[WAIT_FOR_DATA].stop();
			// Start the "waiting for" check interval
			self[WAIT_FOR_DATA].start(name, 'stop');
			if(isAsync(service.stop)){
				callbackify(service.stop).bind(service)(next);
			}else{
				service.stop(next);
			}
		}
		runTeardownHook(service, function (err){
			if(err){
				return 	bail(err);
			}
			return stopService();
		});
	}
	next();
};
RunSV.prototype.async = function (){
	const self = this;
	function AsyncWrapper (){}
	AsyncWrapper.prototype = self;
	let ret = new AsyncWrapper();

	ret.start = util.promisify(self.start).bind(self);
	ret.stop = util.promisify(self.stop).bind(self);
	return Object.freeze(ret);
};

function create (options) {
	return new RunSV(options);
}
exports = module.exports = {
	create
};
