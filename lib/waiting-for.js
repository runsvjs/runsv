'use strict';

function WaitingFor (takingTooLong, emitEvery=100){
	this.interval = null;
	this.callback = takingTooLong;
	this.emitEvery = emitEvery;
}
WaitingFor.prototype.start = function waitingForStart (name, event){
	const { callback, emitEvery } = this;

	let from = Date.now();
	this.interval = setInterval(function (){
		let time = Date.now() - from;
		callback(name, event, time);
	},emitEvery);
};
WaitingFor.prototype.stop = function waitingForStart (){
	if(this.interval){
		clearInterval(this.interval);
	}
};

exports = module.exports = WaitingFor;
