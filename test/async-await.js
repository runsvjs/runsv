'use strict';
const assert = require('assert');
const { promisify } = require('util');
const { create } = require('..');

const wait = promisify(function (callback){
	setImmediate(callback);
});

function createCallbackService (name){
	let client ={
		name,
		started: false,
		stopped: false
	};
	function start (_, callback){
		client.started = true;
		setImmediate(callback);
	}
	function stop (callback){
		client.stopped = true;
		setImmediate(callback);
	}
	function getClient (){
		return client;
	}
	return Object.freeze({
		name, 
		getClient,
		start,
		stop
	});
}

function createAsyncService (name){
	let client ={
		name,
		started: false,
		stopped: false
	};
	async function start (){
		client.started = true;
		await wait();
	}
	async function stop (){
		client.stopped = true;
		await wait();
	}
	function getClient (){
		return client;
	}
	return Object.freeze({
		name, 
		getClient,
		start,
		stop
	});
}

describe('Async/Await support',function (){
	it('should support async start/stop', async function (){
		const runsv = create().async();
		const [a, b] = ['a', 'b'].map(createAsyncService);
		runsv.addService(b);
		runsv.addService(a, b);
		await runsv.start();

		const clientA = runsv.getService('a').getClient();
		assert.deepStrictEqual(clientA.name, 'a');
		assert(clientA.started);

		const clientB = runsv.getService('b').getClient();
		assert.deepStrictEqual(clientB.name, 'b');
		assert(clientB.started);

		await runsv.stop();
		assert(clientA.stopped);
		assert(clientB.stopped);
	});
	it('should support async services', function (done){
		const runsv = create();
		const [a, b] = ['a', 'b'].map(createAsyncService);
		runsv.addService(b);
		runsv.addService(a, b);
		runsv.start(function (err){
			if(err){
				return done(err);
			}
			const clientA = runsv.getService('a').getClient();
			assert.deepStrictEqual(clientA.name, 'a');
			assert(clientA.started);

			const clientB = runsv.getService('b').getClient();
			assert.deepStrictEqual(clientB.name, 'b');
			assert(clientB.started);

			runsv.stop(function (err){
				assert(clientA.stopped);
				assert(clientB.stopped);
				return done(err);
			});
		});
	});
	it('should support a mix of callback/async services',function (done){
		const runsv = create();
		const a = createAsyncService('a');
		const b = createCallbackService('b');
		runsv.addService(a, b);
		runsv.start(function (err){
			if(err){
				return done(err);
			}
			const clientA = runsv.getService('a').getClient();
			assert.deepStrictEqual(clientA.name, 'a');
			assert(clientA.started);

			const clientB = runsv.getService('b').getClient();
			assert.deepStrictEqual(clientB.name, 'b');
			assert(clientB.started);

			runsv.stop(function (err){
				assert(clientA.stopped);
				assert(clientB.stopped);
				return done(err);
			});
		});
	});
});
