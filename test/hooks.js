'use strict';
const assert = require('assert');
const { promisify } = require('util');
const { create } = require('..');

let timers = {};
if(Number(process.versions.node.split('.')[0]) < 16){
// not available in node v12
	timers.setImmediate = promisify(setImmediate);
	timers.setTimeout =  promisify(setTimeout);
}else{
	timers = require('timers/promises');
}


function createService (name, events) {
	return Object.freeze({
		name,
		start (_, callback) {
			events.push([name, 'start']);
			return setImmediate(callback);
		},
		getClient () {
			events.push([name, 'getClient']);
			return {
				name,
				work (...args){
					events.push([name, 'work', args]);
				}
			};
		},
		stop (callback) {
			events.push([name, 'stop']);
			return setImmediate(callback);
		}
	});
}
describe('Feature: Hooks', function () {
	describe('Setup/Teardown hooks', function (){
		before('setup and run', function (done) {
			const self = this;
			self.context = null;
			self.events = [];
			const sv1= createService('sv1', self.events);
			const sv2 = createService('sv2', self.events);
			const sv = create();
			const [_, teardownSv2 ]= sv.addService(sv2);
			const [setupSv1, teardownSv1 ]= sv.addService(sv1, sv2);
			setupSv1(function (ctxt, deps, callback){
				self.events.push(['sv1', 'setup']);
				// shared context
				self.context = self.context || ctxt;
				ctxt.seq  = ctxt.seq || [];
				ctxt.seq.push('sv1:setup');

				deps.sv1.work('setupSV1', deps);
				setTimeout(callback, 1);
			});

			teardownSv1(function (ctxt, deps, callback){
				self.events.push(['sv1', 'teardown']);
				// shared context
				self.context = self.context || ctxt;
				ctxt.seq  = ctxt.seq || [];
				ctxt.seq.push('sv1:teardown');

				deps.sv1.work('teardown1', deps);
				setTimeout(callback, 1);
			});
			teardownSv2(function (ctxt, _, callback){
				self.events.push(['sv2', 'teardown']);
				// shared context
				self.context = self.context || ctxt;
				ctxt.seq  = ctxt.seq || [];
				ctxt.seq.push('sv2:teardown');

				setImmediate(callback);
			});

			sv.on('waiting', (...args) => self.events.push(['wait', ...args]));
			sv.start(function (err, clients) {
				if (err) {
					throw err;
				}
				self.events.push(['all', 'ready']);
				self.clients = clients;
				clients.sv1.work('ready');
				self.events.push(['all', 'stopping']);

				sv.stop(done);
			});
		});
		it('should run setup/teardown hooks in order', function (){
			// if sv1 has a setup and a teardown hook and a dependency on sv2
			// start order will be sv2, sv1, sv1:setup
			// stop order will be sv1:teardown, sv1, sv2
			const { events } = this;
			
			const expected = [
				['sv2','start'], 
				['sv1', 'start'], 
				['sv1', 'setup'],
				['all', 'ready'],
				['all', 'stopping'],
				['sv1', 'teardown'],
				['sv1', 'stop'],
				['sv2', 'stop'],
			];

			for(let [svA, evA] of events){
				const [svB, evB] = expected[0];
				if (svA === svB && evA === evB){
					expected.shift();
				}

			}
			assert.deepStrictEqual([], expected);
		});
		it('should pass the shared context', function (){
			const { context:actual } = this;
			const expected = { 
				seq: [ 
					'sv1:setup', 
					'sv1:teardown', 
					'sv2:teardown' 
				] 
			};
			assert.deepStrictEqual(actual, expected);
		});
	});
	describe('Errors', function (){
		class HookError extends Error {}
		context('When a setup hook throws an error', function (){
			before(function arrangeSetup (done){
				const self = this;
				self.events = [];
				const sv1= createService('sv1', self.events);
				const sv2 = createService('sv2', self.events);
				const sv = create();
				const [setupSv1, teardownSv1] = sv.addService(sv1, sv2);
				setupSv1(function (_ctxt, _deps, callback){
					setImmediate(callback, new HookError('hello'));
				});
				teardownSv1(function (){
					return done(new Error('unexpected code branch'));
				});
				sv.start(function (err){
					self.err =err;
					return done();
				});
			});
			it('should bail', function (){
				const { err } = this;
				assert(err instanceof HookError);
			});
		});
		context('When a teardown hook throws an error', function (){
			before(function arrangeSetup (done){
				const self = this;
				self.events = [];
				const sv1= createService('sv1', self.events);
				const sv2 = createService('sv2', self.events);
				const sv = create();
				const [_, teardownSv1] = sv.addService(sv1, sv2);
				teardownSv1(function (_ctxt, _deps, callback){
					setImmediate(callback, new HookError('hello'));
				});
				sv.start(function (err){
					if(err){
						return done(err);
					}
					sv.stop(function (err){
						self.err = err;
						return done();
					});
				});
			});
			it('should bail', function (){
				const { err } = this;
				assert(err instanceof HookError);
			});
		});
	});
	describe('Hooks taking too long', function (){
		before(function actStartServices (done){
			const self = this;
			self.events = [];
			const sv1= createService('sv1', self.events);
			const sv = create({ emitWaitingEvery:100 });
			sv.on('waiting', (...args) => self.events.push(['wait', ...args]));
			const [setupSv1, teardownSv1] = sv.addService(sv1);
			setupSv1(function (_ctxt, _deps, callback){
				return setTimeout(callback, 300);
			});
			teardownSv1(function (_ctxt, _deps, callback){
				return setTimeout(callback, 300);
			});
			sv.start(function (err){
				if(err){
					return done(err);
				}
				sv.stop(done);
			});
		});
		it('should emit events',function (){
			const { events } = this;
			// we can not predict the amount of events we are going to have
			// but at least we should have 2 of each type
			let sv1Setup = 0;
			let sv1Teardown = 0;

			for(let [type, name]  of events){
				if(type !== 'wait'){
					continue;
				}
				if(name === 'sv1:hook:setup'){
					sv1Setup++;
				}
				if(name === 'sv1:hook:teardown'){
					sv1Teardown++;
				}
			}

			assert(sv1Setup > 1);
			assert(sv1Teardown > 1);
		});
	});
	describe('Promisified hooks', function (){
		before(function actStartServices (done){
			const self = this;
			self.events = [];
			const sv1= createService('sv1', self.events);
			const sv = create({ emitWaitingEvery:100 });
			sv.on('waiting', (...args) => self.events.push(['wait', ...args]));
			const [setupSv1, teardownSv1] = sv.addService(sv1);
			setupSv1(async function (ctxt, deps){
				self.ctxt = ctxt;
				ctxt.setupSv1 = {
					deps: Object.keys(deps),
				};
				const start = Date.now();
				const ret = await timers.setTimeout(300, 'waited:setup');
				ctxt.setupSv1.took = Date.now() -start;
				ctxt.setupSv1.ret = ret;
			});
			teardownSv1(async function (ctxt, deps){
				ctxt.teardownSv1 = { 
					deps: Object.keys(deps),
					prevCtxt : ctxt.setupSv1.ret
				};
				const start = Date.now();
				const ret = await timers.setTimeout(300, 'waited:teardown');
				ctxt.teardownSv1.took = Date.now() -start;
				ctxt.teardownSv1.ret = ret;
			});
			sv.start(function (err){
				if(err){
					return done(err);
				}
				sv.stop(done);
			});
		});
		it('should work as the callback ones', function (){
			const { ctxt, events } = this;
			// check that we really waited
			let sv1Setup = 0;
			let sv1Teardown = 0;

			for(let [type, name]  of events){
				if(type !== 'wait'){
					continue;
				}
				if(name === 'sv1:hook:setup'){
					sv1Setup++;
				}
				if(name === 'sv1:hook:teardown'){
					sv1Teardown++;
				}
			}

			assert(sv1Setup > 1);
			assert(sv1Teardown > 1);
			// check shared context
			assert.deepStrictEqual(ctxt.setupSv1.ret, 'waited:setup');
			assert.deepStrictEqual(ctxt.teardownSv1.ret, 'waited:teardown');
			assert.deepStrictEqual(ctxt.teardownSv1.prevCtxt, 'waited:setup');

		});
	});
});
