'use strict';
const assert = require('assert');
const { create } = require('..');

function createService (name, event) {
	return Object.freeze({
		name,
		start (services, callback) {
			event && event.start(name, services);
			return callback();
		},
		getClient () {
			event && event.getClient && event.getClient(name);
			return { name };
		},
		stop (callback) {
			event && event.stop(name);
			return callback();
		}
	});
}
describe('RunSV', function () {
	describe('basic behavior', function () {
		before('setup and run', function (done) {
			const self = this;
			self.events = {};
			const event = {
				start () { self.events.started = true; },
				stop () { self.events.stopped = true; }
			};
			const myAsyncReadyService = createService('myService', event);
			self.myAsyncReadyService = myAsyncReadyService;
			const sv = create();
			sv.addService(myAsyncReadyService);
			sv.start(function (err, clients) {
				if (err) {
					throw err;
				}
				self.clients = clients;
				sv.stop(done);
			});
		});
		describe('#start(callback)', function (){
			it('should clients in the callback',function (){
				const { myService } = this.clients;
				assert.deepStrictEqual(myService.name, 'myService');
			});
		});
		it('can start a service', function () {
			assert(this.events.started);
		});
		it('can stop a service', function () {
			assert(this.events.stopped);
		});
	});
	describe('execution plan', function () {
		before(function () {
			const runsv = create();
			const [a, b] =	['a', 'b'].map(name => createService(name));
			runsv.addService(a);
			runsv.addService(b, a);
			this.runsv = runsv;
		});
		describe('#startPlan()', function () {
			it('should return service start order', function () {
				const actual = this.runsv.startPlan();
				const expected = ['a', 'b'];
				assert.deepStrictEqual(actual, expected);
			});
		});
		describe('#stopPlan()', function () {
			it('should return service start order', function () {
				const actual = this.runsv.stopPlan();
				const expected = ['b', 'a'];
				assert.deepStrictEqual(actual, expected);
			});
		});
	});
	describe('#getClients([...only])', function () {
		it('should return all clients', function (done) {
			const sv = create();
			['a', 'b'].forEach(x => sv.addService(createService(x)));
			sv.start(function (err) {
				if (err) {
					throw err;
				}
				const clients = sv.getClients();
				assert.deepStrictEqual(Object.keys(clients).sort(), ['a', 'b']);
				assert.deepStrictEqual(clients.a.name, 'a');
				assert.deepStrictEqual(clients.b.name, 'b');
				sv.stop(done);
			});
		});
		it('when clients are specified it should only return those', function (done) {
			const sv = create();
			['a', 'b', 'c'].forEach(x => sv.addService(createService(x)));
			sv.start(function (err) {
				if (err) {
					throw err;
				}
				const clients = sv.getClients('a', 'c');
				assert.deepStrictEqual(Object.keys(clients).sort(), ['a', 'c']);
				assert.deepStrictEqual(clients.a.name, 'a');
				assert.deepStrictEqual(clients.c.name, 'c');
				sv.stop(done);
			});
		});
		it('it should only return existing clients', function (done) {
			const sv = create();
			['a'].forEach(x => sv.addService(createService(x)));
			const c = {
				name: 'c',
				start (_, callback) { callback(); },
				stop (callback) { callback(); }
			};
			delete c.getClient;
			sv.addService(c);
			sv.start(function (err) {
				if (err) {
					throw err;
				}
				const clients = sv.getClients();
				assert.deepStrictEqual(Object.keys(clients).sort(), ['a']);
				assert.deepStrictEqual(clients.a.name, 'a');
				sv.stop(done);
			});
		});
	});
	describe('#getService(name)', function () {
		it('should return that service', function () {
			const [a, b] = ['a', 'b'].map(x => createService(x));
			const sv = create();
			sv.addService(a);
			sv.addService(b);
			const service = sv.getService('b');
			assert(Object.is(service, b));
		});
	});
	describe('#listServices()', function () {
		it('should return a list of services', function () {
			const [a, b] = ['a', 'b'].map(x => createService(x));
			const sv = create();
			sv.addService(a);
			sv.addService(b);
			const actual = sv.listServices().sort();
			assert.deepStrictEqual(actual, ['a', 'b']);
		});
	});
	describe('dependencies', function () {
		before('setup and run', function (done) {
			const self = this;
			self.serviceEvents = { started: [], stopped: [] };
			const serviceEvent = {
				start (name, services) { self.serviceEvents.started.push([name, services]); },
				stop (name) { self.serviceEvents.stopped.push(name); }
			};
			const [a, b] = ['a', 'b'].map(x => createService(x, serviceEvent));
			const sv = create();
			// B depends on A
			sv.addService(b, a);
			sv.start(function (err) {
				if (err) {
					throw err;
				}
				sv.stop(done);
			});
		});
		it('should start a service with dependencies in the proper order', function () {
			assert.deepStrictEqual(this.serviceEvents.started.map(x => x[0]), ['a', 'b']);
		});
		it('should stop a service with dependencies in the proper order', function () {
			assert.deepStrictEqual(this.serviceEvents.stopped, ['b', 'a']);
		});
		describe('A service with dependencies', function () {
			it('should have access their clients', function () {
				const expected = [
					['a', null], // "a" receives no clients
					['b', { a: { name: 'a' } }] // "b" should get access to client of "a"
				];
				assert.deepStrictEqual(this.serviceEvents.started, expected);
			});
		});
		describe('Circular dependencies', function () {
			it('a -> a should fail', function (done) {
				const a = createService('a');
				const sv = create();
				// B depends on A
				sv.addService(a, a);
				sv.start(function (err) {
					assert.deepStrictEqual(err.message, 'Dependency Cycle Found: a -> a');
					done();
				});
			});
			it('a -> b -> c -> a should fail', function (done) {
				const self = this;
				self.serviceEvents = { started: [], stopped: [] };
				const serviceEvent = {
					start (name, services) { self.serviceEvents.started.push([name, services]); },
					stop (name) { self.serviceEvents.stopped.push(name); }
				};
				const [a, b, c] = ['a', 'b', 'c'].map(x => createService(x, serviceEvent));
				const sv = create();
				// B depends on A
				sv.addService(a, b);
				sv.addService(c, a);
				sv.addService(b, c);
				sv.start(function (err) {
					assert.deepStrictEqual(err.message, 'Dependency Cycle Found: a -> b -> c -> a');
					done();
				});
			});
		});
	});
	describe('Start/Stop events', function () {
		const [a, c] = ['a', 'c'].map(x => createService(x));
		const b = {
			name: 'b',
			start (services, callback) {
				return callback(null, { stats: 1 });
			},
			getClient () {
				return { name:'b' };
			},
			stop (callback) {
				return callback();
			}
		};
		before(function (done) {
			const self = this;
			self.start = [];
			self.stop = [];

			const runsv = create();

			runsv.addService(b, a);
			runsv.addService(c, b);

			runsv.on('start', (name, result) => self.start.push([name, result]));
			runsv.on('stop', (name, result) => self.stop.push([name, result]));
			runsv.start(function () {
				runsv.stop(done);
			});
		});
		it('should emit start events (name, took)', function () {
			const [first, second, third] = this.start;
			assert.deepStrictEqual(first[0], 'a');
			assert(first[1].took >= 0);
			assert.deepStrictEqual(second[0], 'b');
			assert(second[1].took >= 0);
			assert.deepStrictEqual(third[0], 'c');
			assert(third[1].took >= 0);
		});
		it('should emit stop events (name, took)', function () {
			const [first, second, third] = this.stop.reverse();
			assert.deepStrictEqual(first[0], 'a');
			assert(first[1].took >= 0);
			assert.deepStrictEqual(second[0], 'b');
			assert(second[1].took >= 0);
			assert.deepStrictEqual(third[0], 'c');
			assert(third[1].took >= 0);
		});
	});
	describe('Start/Stop waiting for events', function () {
		const sleep = 400;
		const [a, b, c] = ['a', 'b', 'c'].map(function (name){
			return {
				name,
				start (services, callback) {
					if(name === 'a'){
						return setTimeout(function (){
							callback(null, { stats:1 });
						}, sleep);
					}
					return callback();
				},
				getClient () {
					return { name };
				},
				stop (callback) {
					if(name === 'b'){
						return setTimeout(function (){
							callback(null, { stats:1 });
						}, sleep);
					}
					return callback();
				}
			};
		});
		before(function (done) {
			const self = this;
			self.events = [];

			const runsv = create({ emitWaitingEvery:100 });

			runsv.addService(b, a);
			runsv.addService(c, b);

			function collector (name){
				return function collect (...args){
					self.events.push([name, ...args]);
				};
			}

			['start', 'stop', 'waiting'].forEach(function (event){
				runsv.on(event, collector(event));
			});

			runsv.start(function () {
				runsv.stop(done);
			});
		});
		it('should emit waiting events (name, event, time)', function () {
			const { events } = this;
			// expect this list of events. There could be some difference in timing
			// so lets just keep the first 2 cols
			const expected = [
				[ 'waiting', 'a', 'start', 101 ],
				[ 'waiting', 'a', 'start', 201 ],
				[ 'waiting', 'a', 'start', 302 ],
				[ 'start', 'a', { stats: 1, took: 402 } ],
				[ 'start', 'b', { took: 0 } ],
				[ 'start', 'c', { took: 0 } ],
				[ 'stop', 'c', { took: 0 } ],
				[ 'waiting', 'b', 'stop', 100 ],
				[ 'waiting', 'b', 'stop', 201 ],
				[ 'waiting', 'b', 'stop', 301 ],
				[ 'stop', 'b', { stats: 1, took: 400 } ],
				[ 'stop', 'a', { took: 0 } ]
			].map(x => x.slice(0,2));
			const actual = events.map(x => x.slice(0,2));
			assert.deepStrictEqual(actual, expected);
		});
	});
	describe('Errors', function () {
		describe('On service start error', function () {
			const expectedError = new Error('ooooops');
			const service = {
				name: 'failOnError',
				start (_, callback) {
					return callback(expectedError);
				},
				stop (callback) {
					callback();
				}
			};
			it('should callback that error', function (done) {
				const sv = create();
				sv.addService(service);
				sv.start(function (err) {
					assert(err === expectedError);
					done();
				});
			});
			it('should be able to tell which service created throwed that error',function (done){
				const expected = 'Could not start failOnError. Reason: ooooops';
				const sv = create();
				sv.addService(service);
				sv.start(function (err) {
					const actual = err.message;
					assert(actual, expected);
					done();
				});
			});
		});
		describe('On service stop error', function () {
			const expectedError = new Error('ooooops');
			const service = {
				name: 'failOnError',
				start (_, callback) {
					callback();
				},
				stop (callback) {
					return callback(expectedError);
				}
			};
			it('should callback that error', function (done) {
				const sv = create();
				sv.addService(service);
				sv.start(function (err) {
					assert(!err, 'unexpected error');
					sv.stop(function (err) {
						assert(err === expectedError);
						done();
					});
				});
			});
			it('should be able to tell which service created throwed that error',function (done){
				const expected = 'Could not stop failOnError. Reason: ooooops';
				const sv = create();
				sv.addService(service);
				sv.start(function (err) {
					assert(!err, 'unexpected error');
					sv.stop(function (err){
						const actual = err.message;
						assert(actual, expected);
						done();
					});
				});
			});
		});
		describe('Defining deps in multipe calls to #addService(...)',function (){
			it('Should make no difference ', function (done){
				const sv = create();
				const expected = ['a', 'b', 'c'];
				const [a, b, c] =	expected.map(name => createService(name));
				// C depends on A & B but deps are declared in different calls to sv.addService
				sv.addService(c, a);
				sv.addService(c, b);
				// we define A with no deps to prevenc circular deps
				sv.addService(b, a);
				const actual = sv.startPlan();
				sv.start(function (err) {
					assert.deepStrictEqual(actual, expected);
					return done(err);
				});
			});
		});
	});
});
